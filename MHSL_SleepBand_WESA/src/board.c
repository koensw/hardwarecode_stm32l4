/**
	Load matching board source
 */
 
#include "board.h"
 
#ifdef STM32L4_NUCLEO
	#include "nucleo_board.c"
#else
	#include "eeg_board.c"
#endif
