/**
	Setup for various interfaces
 */
 
#include "stm32l4xx_hal.h"
#include "stm32l4xx_hal_tim.h"
 
void SystemClock_Config(void);
 
void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi);
void HAL_SPI_MspDeInit(SPI_HandleTypeDef *hspi);

void HAL_TIM_Base_MspInit(TIM_HandleTypeDef* htim);
void HAL_TIM_Base_MspDeInit(TIM_HandleTypeDef *htim);
