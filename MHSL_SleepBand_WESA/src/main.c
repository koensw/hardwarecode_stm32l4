/**
  ******************************************************************************
  * @file    GPIO/GPIO_EXTI/Src/main.c 
  * @author  MCD Application Team
  * @version V1.2.7
  * @date    04-November-2016
  * @brief   This example describes how to configure and use GPIOs through 
  *          the STM32F4xx HAL API.
  ******************************************************************************
  * @attention
  *
  * <h2><center>&copy; COPYRIGHT(c) 2016 STMicroelectronics</center></h2>
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/
#include "main.h"

#include "config.h"
#include "setup.h"

#include "string.h"
#include "stdio.h"

/* Private typedef -----------------------------------------------------------*/
/* Private define ------------------------------------------------------------*/
#define SLAVE_BOARD
	
/* Private macro -------------------------------------------------------------*/
/* Private variables ---------------------------------------------------------*/

/* SPI handler declaration */
SPI_HandleTypeDef SpiHandle;


/* Buffer used for transmission */
uint32_t TXBufferIdx = 0;
uint8_t TXBuffer[BUFFERSIZE];

/* Buffer used for reception */
uint8_t RXBuffer[BUFFERSIZE];
uint32_t RXBufferIdx = 0;;

uint32_t SmpIndx;

uint32_t EEG_Sample;
uint8_t Volume = 50;

// Notch filter initialization
int NF_Csize  = 3; //(2*NF_order)+1
float Num[3] = {0.9598, -0.5932, 0.9598}; //NF_fact = 15
float Den[3] = {1.0000, -0.5932, 0.9195}; //NF_fact = 15
float raw_input_buff[3];
float filterOut_buff[3];


/* Private function prototypes -----------------------------------------------*/

/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
	/** INITIALIZATION */
	
	// <<<<<<<<<<<<<<<<<<<<<<<<<  BEGIN OF VARIABLE INITIALIZATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	//reception buffer initialization
	RXBufferIdx = 0;	
	RXBuffer[1] = 0;
	
	//initialization of HAL SPI
	HAL_Init();
	
	//Configure the system clock to 168 MHz
  SystemClock_Config();
	
	//<<<<<<<<<<<<<<<<<<<<<<<<<  END OF VARIABLE INITIALIZATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
		
	// <<<<<<<<<<<<<<<<<<<<<<<<<  BEGIN OF SPI CONFIGURATION >>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
	/* Configure the SPI peripheral */
  /* Set the SPI parameters */
  SpiHandle.Instance               = SPIx;
  SpiHandle.Init.BaudRatePrescaler = SPI_BAUDRATEPRESCALER_2;
  SpiHandle.Init.Direction         = SPI_DIRECTION_2LINES;
  SpiHandle.Init.CLKPhase          = SPI_PHASE_2EDGE;
  SpiHandle.Init.CLKPolarity       = SPI_POLARITY_LOW;
  SpiHandle.Init.CRCCalculation    = SPI_CRCCALCULATION_DISABLE;
  SpiHandle.Init.CRCPolynomial     = 7;
  SpiHandle.Init.DataSize          = SPI_DATASIZE_8BIT;
  SpiHandle.Init.FirstBit          = SPI_FIRSTBIT_MSB;
  SpiHandle.Init.NSS               = SPI_NSS_HARD_INPUT;
  SpiHandle.Init.TIMode            = SPI_TIMODE_DISABLE;	
	SpiHandle.Init.Mode 						 = SPI_MODE_SLAVE;

	if(HAL_SPI_Init(&SpiHandle) != HAL_OK)
  {    
    Error_Handler(); // Initialization Error 
  }

	/** RUN */
	
  /* Configure LEDs */
  BSP_LED_Init(LED_RED);
  BSP_LED_Init(LED_GREEN);

	/* Keep toggling LED */
	while (1){
		BSP_LED_Toggle(LED_RED);
		int i=1e6;
		while (i-->0) i--;
  }
}

/**
  * @brief  SPI error callbacks
  * @param  hspi: SPI handle
  * @note   Simple way to report transfer error
  * @retval None
  */
 void HAL_SPI_ErrorCallback(SPI_HandleTypeDef *hspi){
  /* Turn LED4 on: Transfer error in reception/transmission process */
  BSP_LED_On(LED_RED); 
}

/**
  * @brief  This function is executed in case of error occurrence.
  * @param  None
  * @retval None
  */
void Error_Handler(void)
{
  /* USER CODE BEGIN Error_Handler */
  /* User can add his own implementation to report the HAL error return state */
	while (1){		    
		BSP_LED_Toggle(LED_RED);
		int i=1e6;
		while (i-->0) i--;
  }
  /* USER CODE END Error_Handler */ 
}

#ifdef  USE_FULL_ASSERT

/**
  * @brief  Reports the name of the source file and the source line number
  *         where the assert_param error has occurred.
  * @param  file: pointer to the source file name
  * @param  line: assert_param error line source number
  * @retval None
  */
void assert_failed(uint8_t* file, uint32_t line)
{ 
  /* Infinite loop */
  while (1) {}
}
#endif

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
