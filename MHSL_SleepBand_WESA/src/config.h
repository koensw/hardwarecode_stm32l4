/*
 * config.h
 *
 *  Created on: 23.06.2017
 *      Author: fersterm
 */
#include <math.h>
#ifndef CONFIG_H_
#define CONFIG_H_

/******************************/
/* General definition         */
/******************************/
#define SAMPLE_FREQ    		250
#define ADS1299_Vref     	4.5
#define ADS1299_gain    	24.0
#define Hexa_POS_NEG   		16777216
#define POS_TO_NEG_LIM		8388607
#define SAMPLE_TO_uVOLTS 	0.02235174445 //(ADS1299_Vref / 8388607.0 / ADS1299_gain * 1000000)


/******************************/
/* Testing and validation     */
/******************************/
#define SWD_VALIDATION			1  // Set SWD_VALIDATION to 1 to run all the algortihms independently

/******************************/
/*     Phase-locked loop      */
/******************************/
//#define GAIN_PD    			2.5
//#define GAIN_NCO   			0.095
//#define TARGET_PHASE   	(M_PI/4-M_PI/2)

#define GAIN_PD    			1000
#define GAIN_NCO   			0.095
#define TARGET_PHASE   	(-M_PI/4)

#define CENTRAL_FREQ 		1		// Central NCO frequency at 1Hz
#define Wc 							(2*M_PI*CENTRAL_FREQ/SAMPLE_FREQ)
#define HIGHPASS_FREQ	  0.1


/******************************/
/* SLOW WAVE DETECTOR         */
/******************************/
#define SW_THRESHOLD 					    -40
#define SW_UP_THRESHOLD 			    -300 	// If wave is smaller than this threshold, then it is considered an arausal
#define FREQ_LOW					 		     0.5	// in Hz
#define FREQ_HIGH		 		     			 2		// in Hz
#define FREQ_CONDITION_LOW_LIM  	 FREQ_LOW*SAMPLE_FREQ/2 	// Half period of a 0.5Hz wave
#define FREQ_CONDITION_HIGH_LIM 	 FREQ_HIGH*SAMPLE_FREQ/2 	// Half period of a 2Hz wave


//** Slow Oscilation (SO) cutoff frequencies
// kispi filter: use hard filter configuration. See filters.h
#define LowFcSO_KISPI				0.319825
#define HighFcSO_KISPI			3.12648
#define FiltOrder_KISPI			3

// MHSL filter
#define SO_FcLow					0.2
#define SO_FcHigh					5
#define SO_Order					2
#define SO_CoeffOrder	  	(2*SO_Order)+1

// After detecting Sleep, wait until NREM sleep is stable
#define SW_DETECTION_DELAY_IN_SEC	0//(60*3-10)
#define SW_DETECTION_DELAY			SW_DETECTION_DELAY_IN_SEC*SAMPLE_FREQ
#define SO_FILTERING_DELAY			0//10*SAMPLE_FREQ

/******************************/
/*Sleep detection by Caroline*/
/******************************/
// Frequency bands
#define LowFcDelta2			2
#define HighFcDelta2		4
#define LowFcDelta4			3
#define HighFcDelta4		5
#define LowFcMuscPow2		20
#define HighFcMuscPow2	30


//config for 80 sec epoch
#define POW_CALC_WIND_SEC 	4
#define POW_CALC_WIND     	(POW_CALC_WIND_SEC*SAMPLE_FREQ)
#define AVG_CALC_WIND_SEC 	80
#define AVG_CALC_SAMP  			(AVG_CALC_WIND_SEC/POW_CALC_WIND_SEC)
#define AVG_GAIN						4


// Thresholds acording to population: "Y": for young adults. "E": for elderly
#define POPULATION 			0

// Elderly
#define E_D4_OVER_B2_TH 	4.5
#define E_B2_TH						24
#define E_Muscle_TH				16

// Young adults
#define Y_D4_OVER_M_TH	 	7
#define Y_D2_TH						76
#define Y_Muscle_TH				16


/******************************/
/*         AUDIO              */
/******************************/
#define TONE_DURATION_SAMP		13  // duration in time = TONE_DURATION_SAMP/fs
#define AUDIO_VOLUME_ON				50  // Volume level (from 0 (Mute) to 100 (Max)) 
#define AUDIO_VOLUME_STEP			 2
#define AUDIO_VOLUME_OFF			 0
#define AUDIO_FREQ						22050 // I2SFreq[8] = {8000, 11025, 16000, 22050, 32000, 44100, 48000, 96000}

//#define AUDIO_BUFFER_SIZE_

/*#define TONE_DURATION		0.05  // 50 ms duration
#define AUDIO_FREQUENCY		16000 // 16 KHZ
#define AUDIO_VOLUME		35
#define TONE_TYPE			"PINK_NOISE"

#define TONE_DURATION_IN_SAMP	TONE_DURATION*AUDIO_FREQUENCY
*/

/******************************/
/*   HARD CONDITIONS          */
/******************************/

#define INITIAL_SLEEP_ONSET_TIME_SAMP  10*60*SAMPLE_FREQ //




#endif /* CONFIG_H_ */
